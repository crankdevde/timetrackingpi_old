var app = angular.module('timetrackingpi', []);
app
.config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
})
// Factorys
.factory('authInterceptor',[ '$rootScope','$q','$window', function ($rootScope, $q, $window) {
  var isAuth = false;
  var user = {};
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        isAuth=true;
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
        config.headers.User = $window.sessionStorage.username;
      }
      return config;
    },
    responseError: function (rejection) {
      if (rejection.status === 401) {
        // handle the case where the user is not authenticated
        delete $window.sessionStorage.token;
        isAuth=false;
      }
      return $q.reject(rejection);
    },
    getAuth: function(){
      return isAuth;
    },
    setAuth: function(bole){
      isAuth=bole;
    },
    getUser: function(){
      return user;
    },
    setUser: function(User){
      user=User;
    }
  };
}]);



app.controller('ttpCtrl', ['$scope','$http','$window','authInterceptor', function($scope, $http, $window, authInterceptor) {
	$scope.isAuthenticated = false;
	$scope.viewForm = false;
	$scope.viewTimes = true;
	if ($window.sessionStorage.token) {
		$scope.isAuthenticated = true;
		$scope.viewForm = false;
		$scope.viewTimes = true;
	}
	$scope.viewSection = 'index';

	checkSN($scope, $http);
	$http.get("/api/list").then(function (response) {

        $scope.dbrows = sortList(response.data);
        $scope.orderedTimes = {};
        for (var user in $scope.dbrows) {
		    // skip loop if the property is from prototype
		    if ( ! $scope.dbrows.hasOwnProperty(user) ) continue;

	        $scope.orderedEmpTime = Object.keys($scope.dbrows[user].time)
	            .map(function (value, index) {
	                return { orderedDate: new Date(value), values: $scope.dbrows[user].time[value] }
	            });
	        $scope.orderedTimes[user] = {};   
	        $scope.orderedTimes[user]['ordered'] = $scope.orderedEmpTime;
	        $scope.orderedTimes[user].sn = user;
	        $scope.orderedTimes[user].lastLog = $scope.dbrows[user].lastLog;
		}
		//console.log($scope.orderedTimes);
	});
	$scope.logout = function () {
	    $scope.isAuthenticated = false;
	    delete $window.sessionStorage.token;
	    authInterceptor.setAuth(false);
		$window.location.reload();
	  };

	$scope.submit = function () {
    $http
      .post('/login', $scope.user)
      .success(function (data, status, headers, config) {
        var encodedProfile = data.token.split('.')[1];
        $window.sessionStorage.token = data.token;
        $scope.isAuthenticated = true;
        authInterceptor.setAuth(true);
		$window.location.reload();
      })
      .error(function (data, status, headers, config) {
        // Erase the token if the user fails to log in
        delete $window.sessionStorage.token;
        $scope.isAuthenticated = false;
        authInterceptor.setAuth(false);
        // Handle login errors here
        $scope.error = 'Error: Invalid user or password';
      });
  	};

	$http.get("/api/employee").then(function (response) {
		var employeeStruct = {};
		response.data.forEach(function(row) {
			employeeStruct[row.sn] = row;
		});
        $scope.employee = employeeStruct;
		console.log(employeeStruct);
	});
}])
.controller('EmployeeCtrl', ['$scope', '$http','$window','authInterceptor', function($scope, $http, $window, authInterceptor) {
	$scope.master = {};
	$scope.viewForm = false;
	$scope.viewTimes = true;

	$scope.update = function(user) {
		$scope.master = angular.copy(user);
		$http.post("/api/employee",user).then(function (response) {
		    $scope.user = response.data;
			$scope.viewForm = false;
			$scope.viewTimes = true;
			$window.location.reload();
		});
	};

	$scope.reset = function() {
	$scope.user = angular.copy($scope.master);
	};

	$scope.reset();
}]);

function checkSN($scope, $http) {
	setTimeout(function(){
		$http.get("/api/list").then(function (response) {
	        $scope.dbrows = sortList(response.data);
		});
		$http.get("/api/sn").then(function (response) {
	        $scope.sN = response.data;
	        checkSN($scope, $http);
		});
	},1000);
};

function timeDifference( d1, d2, delta) {
	if(!delta) delta = 0;
	
	if (delta == 0) {
		date1 = new Date(d1);
		date2 = new Date(d2);

	    // get total seconds between the times
		var delta = Math.abs(date1 - date2) / 1000;
	
	} else {
		delta /= 1000;
	}
	// calculate (and subtract) whole days
	var days = Math.floor(delta / 86400);
	delta -= days * 86400;

	// calculate (and subtract) whole hours
	var hours = Math.floor(delta / 3600) % 24;
	delta -= hours * 3600;

	// calculate (and subtract) whole minutes
	var minutes = Math.floor(delta / 60) % 60;
	delta -= minutes * 60;

	// what's left is seconds
	var seconds = Math.round(delta % 60);

    return {day:days,hour:hours,minute:minutes, second: seconds};
};

function sortList(arrayData){
	var newData = {};
	var oldValue={};
	arrayData.forEach(function(row,index){
		if (!newData[row.sn]){
			newData[row.sn] = {};
			myIndex = 0;
		}
		if ( ! newData[row.sn]['time'] ) {
			newData[row.sn]['time'] = {};
			newData[row.sn]['sn'] = row.sn;
			newData[row.sn]['lastLog'] = row.time;
			newData[row.sn]['total'] = {day:0,hour:0,minute:0,second:0,miliseconds:0};
		}
		if ( ! newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")] ) {
			newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")] = {};
		}
		if ( ! newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['data'] ) {
			newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['data'] = [];
		}
		if ( ! newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['total'] ) {
			newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['total'] = {day:0,hour:0,minute:0,second:0,miliseconds:0};
		}

		if ( (myIndex)%2 ) {

			var worked = timeDifference(row.time,oldValue[row.sn].time);

			//first calculate in miliseconds
			var diffMiliseconds = row.time - oldValue[row.sn].time;
			newData[row.sn]['total'].miliseconds += diffMiliseconds;
		
			newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['total'].miliseconds += diffMiliseconds;

			newData[row.sn]['time'][dateFormat(row.time, "yyyy-mm-dd")]['data'].push({timeEnd:row.time,timeStart: oldValue[row.sn].time,worked:worked});
			
		}

		oldValue[row.sn] = row;
		newData[row.sn]['lastLog'] = row.time;
		myIndex++;

	});

	for (var key in newData) {
	    // skip loop if the property is from prototype
	    if ( ! newData.hasOwnProperty(key) ) continue;

	    var obj = newData[key];
    	for ( var prop in obj.time ) {
	        // skip loop if the property is from prototype
	        if( ! obj.time.hasOwnProperty(prop) ) continue;

	        obj.time[prop]['total'] = timeDifference(0,0, obj.time[prop]['total'].miliseconds);
	    }

    	obj['total'] = timeDifference(0,0,obj['total'].miliseconds);
	}

	return newData;
};