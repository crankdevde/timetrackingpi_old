var express = require('express')
  , SerialPort = require('serialport')
  , Datastore = require('nedb')
  , dbTime = new Datastore({ filename: 'time.db', autoload: true })
  , dbEmployee = new Datastore({ filename: 'employee.db', autoload: true })
  , dbDyndns = new Datastore({ filename: 'dyndns.db', autoload: true })
  , dbUser = new Datastore({ filename: 'user.db', autoload: true })
  , myPort
  , sN = ''
  , readerStatus=''
  , request = require('request');

// Set up an express server (not starting it yet)
var server = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressJwt = require('express-jwt');
var jwt = require('jsonwebtoken');
var session = require('express-session')

// configure app to use bodyParser()
// this will let us get the data from a POST
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization,User');

    next();
}
var secret = '09qrjjwef923jnrge$5ndjwk';

server.use(allowCrossDomain);
server.use(cookieParser());
server.use(session({
  secret: secret,
  profile: {},
  apiKey: [],
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}));

// We are going to protect /api routes with JWT
server.use('/api', expressJwt({secret: secret}));

server.use(bodyParser.json());
server.use(bodyParser.urlencoded());
// Use our 'www' folder as rootfolder
server.use('/', express.static(__dirname + '/www/')); // local
// server.use(express.static('/home/pi/timetrackingpi/www')); // pi
server.use(function(err, req, res, next){
  if (err.constructor.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized');
  }
});

// Because I like HTML5 pushstate .. this redirects everything back to our index.html
server.get('/api/sn', function(req, res) {
  res.json({sn:sN,status:readerStatus});
});
server.get('/api/list', function(req, res) {
	dbTime.find({}).sort({sn:1,time:1}).exec(function(err,docs) {
		if (err) console.log(err);
		res.json(docs);
	});
});
server.post('/api/employee', function(req, res) {
	var doc = {};
	doc.firstname = req.body.firstname;
	doc.lastname = req.body.lastname;
	doc.sn = req.body.sn;
	doc.gender = req.body.gender;
	dbEmployee.insert(doc,function(err,newDoc){
		if( err ) console.log(err);
		res.json(newDoc);
	});
})
.get('/api/employee', function(req, res) {
	dbEmployee.find({},function(err,docs){
		if( err ) console.log(err);
		res.json(docs);
	});
});
server
.get('/api/rfid', function(req, res) {
	rfid();
});

server.post('/login', function(req,res){
  //if is invalid, return 401
    if (!(req.body.username === 'pi' && req.body.password === 'timetracking')) {
      res.send(401, 'Wrong user or password');
      return;
    }
    req.session.user = req.body;
    // We are sending the doc inside the token
    var token = jwt.sign(req.body, secret, { expiresIn : 60*60*24 });
    res.json({ token: token });

});

server
.post('/api/dyndns', function(req, res) {
	var doc = {};
	doc.url = req.body.firstname;
	doc.port = req.body.firstname;
	doc.user = req.body.sn;
	doc.password = req.body.gender;
	dbDyndns.insert(doc,function(err,newDoc){
		if( err ) console.log(err);
		res.json(newDoc);
	});
});
server.all('/', function(req, res) {
  res.sendFile('/home/pi/timetrackingpi/www/index.html', { root: 'www' });
});
server.listen(8080, function () {
  console.log('Example app listening on port 8080!')
});
reloadDynDns();
rfid();
function rfid(){
// list serial ports:
SerialPort.list(function (err, ports) {
  if ( err ) console.log(err);
  ports.forEach(function(port) {
  	if ( port.manufacturer == 'wch.cn' || port.manufacturer == '1a86') {
  		console.log(port);
	    myPort = new SerialPort(port.comName, {
		   baudRate: 9600
		});

	    myPort.on('open', showPortOpen);
		myPort.on('data', sendSerialData);
		myPort.on('close', showPortClose);
		myPort.on('error', showError);
	}
	function showPortOpen() {
	   console.log('port open. Data rate: ' + myPort.options.baudRate);
	   readerStatus = {status:200,message:'connected'};
	   loop();
	}
	 
	function sendSerialData(data) {

	   if( data.length == 7 ) {
			if ( sN == '' ){
				sNtmp = data.toString('hex',2,6);
				if ( sNtmp != '03030e03') {
					sN = sNtmp;
					myPort.write('\x02\x13\x15'); // sound
					var doc = { sn: sN, time: Date.now()};
					dbTime.insert(doc,function(err,newDoc){
						if( err ) console.log(err);
					});
				}
				clearReader();
			}
		}
	}
	 
	function showPortClose() {
	   console.log('port closed.');
	   myPort = null;
	   readerStatus = {status:404,message:'connected'};
	   reloadRfid();
	}
	 
	function showError(error) {
	   console.log('Serial port error: ' + error);
	   myPort = null;
	   readerStatus = {status:500,message:error};
	   reloadRfid();
	}

	function loop(){
		if ( myPort ) {
			setTimeout(function(){
			   if ( myPort !== null ) {
				    myPort.write('\x03\x02\x00\x05');
					myPort.write('\x02\x03\x05');
					loop();
				}
			},250);
		}
	}


	function clearReader(){
		setTimeout(function(){

	   		console.log(sN);
		   sN='';
	   },3000);
	}


  });
  if ( !myPort ) {
  	reloadRfid();
  }
});
}

function reloadRfid(){
	rfid();
}

function reloadDynDns(){
	request('https://carol.selfhost.de/update?username=157659&password=1o2n3k4o', function (error, response, body) {
	    if (!error && response.statusCode == 200) {
	        console.log(body) // Print the google web page.
	     }
	});
	setTimeout(function(){
	   reloadDynDns();
   },43200000);
}

