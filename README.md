# TimeTracking PI older Version#
* Welcome

### What is this repository for? ###

Time Tracking Pi is a small NodeJS Application to track time with a RaspberryPi and RFID Reader.
Version: 0.1.3

### How do I get set up? ###

git clone
npm install
node index.js

### Who do I talk to? ###

Slack: https://crankzone.slack.com/signup
Discord: https://discord.gg/vNyfw

Get a Patron on Patreon https://www.patreon.com/deronkozockt
You can Donate on Paypal muellermh.media@gmail.com